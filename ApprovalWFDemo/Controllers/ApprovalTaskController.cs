﻿using System;
using System.Activities;
using System.Activities.DurableInstancing;
using System.Activities.XamlIntegration;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Runtime.DurableInstancing;
using System.Web.Mvc;
using System.Xaml;
using ApprovalWFDemo.Models;
using ApprovalWFDemo.ViewModels;
using ApprovalWFDemo.WorkFlow;
using Microsoft.AspNet.Identity;
using Microsoft.Activities.Extensions.Tracking;
using Newtonsoft.Json.Linq;

namespace ApprovalWFDemo.Controllers
{
    [Authorize]
    public class ApprovalTaskController : Controller
    {
        // GET: ApprovalItem
        
        public ActionResult MyApprovalItems()
        {
            var user = User.Identity.GetUserName();
            var result = new List<ApprovalItemViewModel>();
            using (var db = new BusinessDbContext())
            {
              var items = db.ApprovalItems.Where(x => x.Drafter == user).ToList();
                foreach (var approvalItem in items)
                {
                    var workFlowItme = db.WorkFlowItems.FirstOrDefault(x => x.WfBusinessId == approvalItem.Id);
                    result.Add(new ApprovalItemViewModel
                    {
                        Id = approvalItem.Id,
                        Title = approvalItem.Title,
                        Type = workFlowItme.WfType,
                        Status = workFlowItme.Wfstatus,
                        Applier = approvalItem.Applicant,
                        ApplyDate = approvalItem.CreateDate,
                        CompleteDate = approvalItem.CompleteDate,
                        Drafter = approvalItem.Drafter,
                    });
                }
            }
            return View(result);
        }

        public ActionResult ViewApprovalItem(int approvalId)
        {
            var model = new ApprovalItem();
            using (var db = new BusinessDbContext())
            {
                model = db.ApprovalItems.FirstOrDefault(x => x.Id == approvalId);
                if (model != null)
                {
                    var wf = db.WorkFlowItems.FirstOrDefault(i => i.WfBusinessId == model.Id);
                    ViewBag.WfWriteField = wf.WfWriteField;
                    ViewBag.WfFlowChart = wf.WfFlowChart;
                    ViewBag.instanceId = wf.WfInstanceId;
                }
            }
          
            return View(model);
        }

        public ActionResult MyWorkFlow()
        {
            var result = new List<WorkFlowViewModel>();
            var userName = User.Identity.GetUserName();
            using (var db = new BusinessDbContext())
            {
                var workFlowItems = db.WorkFlowItems.Where(x => x.WfCurrentUser == userName).ToList();
                var businessId = workFlowItems.Select(x => x.WfBusinessId).Distinct().ToList();
                var approvalItems = db.ApprovalItems.Where(x => businessId.Contains(x.Id)).ToList();
                foreach (var workFlowItem in workFlowItems)
                {
                    var curApprovalItem = approvalItems.FirstOrDefault(x => x.Id == workFlowItem.WfBusinessId);
                    result.Add(new WorkFlowViewModel {
                        Id = workFlowItem.WfBusinessId,
                        Title = curApprovalItem.Title,
                        WorkFlowType = workFlowItem.WfType,
                        ApplicantDate = curApprovalItem.CreateDate,
                        Applicant = curApprovalItem.Applicant,
                        ApplicantPhone = curApprovalItem.ApplicantPhone,
                        WfStatus = workFlowItem.Wfstatus,
                        WfDrafter = workFlowItem.WfDrafter,
                        WfBusinessUrl = workFlowItem.WfBussinessUrl
                    });
                }
            }
            return View(result);
        }

        public ActionResult CreateApprovalItem()
        {
            return View();
        }

        //起草审批任务，并持久化
        [Authorize]
        public JObject CreateWorkFlow()
        {
            var syncEvent = new AutoResetEvent(false);

            string currentUserId = User.Identity.GetUserId();
            var user = new ApplicationDbContext().Users.FirstOrDefault(x => x.Id == currentUserId);
            var wfinData = new WorkFlowInParameter
            {
                Drafter = user.UserName
            };
            var inputs = new Dictionary<string, object>();
            inputs.Add("WfIn", wfinData);

            var activity = CreateActivityFrom(Server.MapPath("~") + "\\WorkFlow\\ApprovalWorkFlow.xaml");
            var wfApp = new WorkflowApplication(new ApprovalWorkFlow(), inputs)
            {
                //创建持久化的存储位置
                InstanceStore = CreateInstanceStore(),
                PersistableIdle = delegate(WorkflowApplicationIdleEventArgs e)
                {
                    return PersistableIdleAction.Unload;
                },
                Completed = delegate(WorkflowApplicationCompletedEventArgs e)
                {

                },
                Aborted = delegate(WorkflowApplicationAbortedEventArgs e)
                {
                },
                Unloaded = delegate(WorkflowApplicationEventArgs e)
                {
                    syncEvent.Set();
                },
                OnUnhandledException = delegate(WorkflowApplicationUnhandledExceptionEventArgs e)
                {
                    return UnhandledExceptionAction.Terminate;
                },
                Idle = delegate(WorkflowApplicationIdleEventArgs e)
                {
                }
            };
            //追踪当前 workflow的运行状态
            var stateTracker = new StateMachineStateTracker(wfApp.WorkflowDefinition);
            wfApp.Extensions.Add(stateTracker);
            wfApp.Extensions.Add(new StateTrackerPersistenceProvider(stateTracker));
            var customTracker = new CustomTrackingParticipant(); // 获取activity 内部的变量
            wfApp.Extensions.Add(customTracker);
            Guid instanceId = wfApp.Id;
            wfApp.Run();
            syncEvent.WaitOne();
            var pt = stateTracker.PossibleTransitions;

            string[] strs = pt.Split(',');
            JObject json = new JObject(
                new JProperty("instanceId", instanceId),
                new JProperty("rows",
                    new JArray(
                        from r in strs
                        select new JObject(
                            new JProperty("ID", r.ToString()),
                            new JProperty("Name", r.ToString())))));
            return json;
        }

        public ActionResult DrafterToNextState(ApprovalItem approvalItem, Guid instanceId, string nextLink) //起草者到下一环节
        {
            AutoResetEvent syncEvent = new AutoResetEvent(false);

            WorkflowApplication wfApp = new WorkflowApplication(new ApprovalWorkFlow())
            {

                InstanceStore = CreateInstanceStore(),
                PersistableIdle = delegate(WorkflowApplicationIdleEventArgs e)
                {
                    return PersistableIdleAction.Unload;
                },
                Completed = delegate(WorkflowApplicationCompletedEventArgs e)
                {
                },
                Aborted = delegate(WorkflowApplicationAbortedEventArgs e)
                {
                },
                Unloaded = delegate(WorkflowApplicationEventArgs e)
                {
                    syncEvent.Set();
                },
                OnUnhandledException = delegate(WorkflowApplicationUnhandledExceptionEventArgs e)
                {
                    return UnhandledExceptionAction.Terminate;
                },
                Idle = delegate(WorkflowApplicationIdleEventArgs e)
                {
                }

            };
            var stateTracker = new StateMachineStateTracker(wfApp.WorkflowDefinition); //当前状态追踪
            wfApp.Extensions.Add(stateTracker);
            wfApp.Extensions.Add(new StateTrackerPersistenceProvider(stateTracker));
            var cu = new CustomTrackingParticipant(); //获取Activity内部变量
            wfApp.Extensions.Add(cu); //获取Activity内部变量需要的追踪器
            //Guid instanceId = wfApp.Id;
            var trackerInstance = StateMachineStateTracker.LoadInstance(instanceId, wfApp.WorkflowDefinition,
                ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            wfApp.Load(instanceId);
            BookmarkResumptionResult result = wfApp.ResumeBookmark(trackerInstance.CurrentState, nextLink.Trim());
            syncEvent.WaitOne();
            string CurrentUser = cu.Outputs["CurrentUser"].ToString();
            string OpinionField = cu.Outputs["OpinionField"].ToString();
            string Drafter = cu.Outputs["Drafter"].ToString();
            var CurrentState = stateTracker.CurrentState;
            //var Pt = StateTracker.PossibleTransitions;
            ApplicationUser user =
                    new ApplicationDbContext().Users.Include(i => i.Roles).FirstOrDefault(i => i.UserName == Drafter);
                //获取当前用户username
            approvalItem.Drafter = Drafter;
            approvalItem.CreateDate = DateTime.Now;
            using (var db = new BusinessDbContext())
            {
                db.ApprovalItems.Add(approvalItem);
                try
                {
                    db.SaveChanges();
                }
                catch 
                {
                    var json = new
                    {
                        errorMsg = "添加业务数据出错"
                    };
                    return Json(json, "text/html", JsonRequestBehavior.AllowGet);
                }
           
            WorkFlowItem wf = new WorkFlowItem
            {
                WfInstanceId = instanceId,
                WfType = "IT服务申请",
                WfCurrentUser = CurrentUser,
                WfDrafter = Drafter,
                WfWriteField = OpinionField,
                Wfstatus = CurrentState,
                WfBussinessUrl = "/ApprovalTask/OpenWorkFlow?instanceId=" + instanceId,
                WfCreateDate = DateTime.Now,
                WfBusinessId = approvalItem.Id,
                WfFlowChart = trackerInstance.CurrentState + "(" + user.UserName + ")"
            };
            //添加业务数据关联
            db.WorkFlowItems.Add(wf);
            try
            {
                db.SaveChanges();
                var json = new
                {
                    okMsg = "流程保存成功"
                };

                return Json(json, "text/html", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                var json = new
                {
                    errorMsg = "流程保存出错"
                };

                return Json(json, "text/html", JsonRequestBehavior.AllowGet);
            }
            }
        }


        public ActionResult OpenWorkFlow(Guid instanceId)
        {
            using (var db = new BusinessDbContext())
            {
                var workflowItem = db.WorkFlowItems.FirstOrDefault(x => x.WfInstanceId == instanceId);
                var curUserName = User.Identity.GetUserName();
                if (workflowItem.WfCurrentUser != curUserName)
                {
                    var json = new
                    {
                        errorMsg = "你不是当前处理人"
                    };

                    return Json(json, "text/html", JsonRequestBehavior.AllowGet);
                }
                var bid = workflowItem.WfBusinessId;
                ViewBag.WfWriteField = workflowItem.WfWriteField;
                ViewBag.WfFlowChart = workflowItem.WfFlowChart;
                ViewBag.instanceId = workflowItem.WfInstanceId;
                var  approvalItem = db.ApprovalItems.FirstOrDefault(i => i.Id == bid);
                return View(approvalItem);
            }
        }

        public JObject GetPossiblePath(Guid id)
        {
            WorkflowApplication wfApp = new WorkflowApplication(new ApprovalWorkFlow())
            {

                InstanceStore = CreateInstanceStore(),
                PersistableIdle = delegate (WorkflowApplicationIdleEventArgs e)
                {
                    return PersistableIdleAction.Unload;
                },
                Completed = delegate (WorkflowApplicationCompletedEventArgs e)
                {

                },
                Aborted = delegate (WorkflowApplicationAbortedEventArgs e)
                {
                },
                Unloaded = delegate (WorkflowApplicationEventArgs e)
                {
                },
                OnUnhandledException = delegate (WorkflowApplicationUnhandledExceptionEventArgs e)
                {
                    return UnhandledExceptionAction.Terminate;
                },
                Idle = delegate (WorkflowApplicationIdleEventArgs e)
                {
                }

            };
            var trackerInstance = StateMachineStateTracker.LoadInstance(id, wfApp.WorkflowDefinition, ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            var Pt = trackerInstance.PossibleTransitions;

            //BookmarkResumptionResult result = wfApp.ResumeBookmark("Hello123", "ddd");
            string[] strs = Pt.Split(',');
            JObject json = new JObject(
                new JProperty("rows",
                new JArray(
                     from r in strs
                     select new JObject(
           new JProperty("ID", r.ToString()),
           new JProperty("Name", r.ToString())))));
            return json;
        }
        public ActionResult ToNextState(ApprovalItem isi, Guid instanceId, string nextLink, string Opinion)  //审批者到下一环节,思路：保存当前流程的数据，恢复bookmark到下一环节，并保存下一环节流程信息
        {
            #region 判断是不是当前处理人

            var username = User.Identity.GetUserName();
            using (var db = new BusinessDbContext())
            {
                var workFlowItem = db.WorkFlowItems.FirstOrDefault(x => x.WfInstanceId == instanceId);
                if (workFlowItem.WfCurrentUser.ToString().Trim() != username.Trim())
                {
                    var json = new
                    {
                        errorMsg = "你不是当前处理人"
                    };

                    return Json(json, "text/html", JsonRequestBehavior.AllowGet);
                }
            
            
            #endregion
            AutoResetEvent syncEvent = new AutoResetEvent(false);
            int isComplete = 0;
            WorkflowApplication wfApp = new WorkflowApplication(new ApprovalWorkFlow())
            {

                InstanceStore = CreateInstanceStore(),
                PersistableIdle = delegate (WorkflowApplicationIdleEventArgs e)
                {
                    //var ex = e.GetInstanceExtensions<CustomTrackingParticipant>();
                    // Outputs = ex.First().Outputs.ToString();
                    return PersistableIdleAction.Unload;
                },
                Completed = delegate (WorkflowApplicationCompletedEventArgs e)
                {
                    isComplete = 1;
                    syncEvent.Set();
                },
                Aborted = delegate (WorkflowApplicationAbortedEventArgs e)
                {
                },
                Unloaded = delegate (WorkflowApplicationEventArgs e)
                {
                    syncEvent.Set();
                },
                OnUnhandledException = delegate (WorkflowApplicationUnhandledExceptionEventArgs e)
                {
                    return UnhandledExceptionAction.Terminate;
                },
                Idle = delegate (WorkflowApplicationIdleEventArgs e)
                {
                }

            };
            var StateTracker = new StateMachineStateTracker(wfApp.WorkflowDefinition); //当前状态追踪
            wfApp.Extensions.Add(StateTracker);
            wfApp.Extensions.Add(new StateTrackerPersistenceProvider(StateTracker));
            var cu = new CustomTrackingParticipant();           //获取Activity内部变量
            wfApp.Extensions.Add(cu);                         //获取Activity内部变量需要的追踪器
            //Guid instanceId = wfApp.Id;
            var trackerInstance = StateMachineStateTracker.LoadInstance(instanceId, wfApp.WorkflowDefinition, ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            wfApp.Load(instanceId);

            BookmarkResumptionResult result = wfApp.ResumeBookmark(trackerInstance.CurrentState, nextLink.Trim());   //恢复当前状态,并进入下一个bookmark,注意使用Trim,开始没使用，NextLInk无法取到，调试了大半夜
            syncEvent.WaitOne();

            string CurrentUser;
            string OpinionField = "";
            string CurrentState;
            string completeStr = "";

            if (isComplete == 0)
            {
                CurrentUser = cu.Outputs["CurrentUser"].ToString();
                OpinionField = cu.Outputs["OpinionField"].ToString();
                CurrentState = StateTracker.CurrentState;
            }
            else
            {
                CurrentUser = "System";
                CurrentState = "已结束";
                completeStr = "->结束";
            }
            //string currentUserId = User.Identity.GetUserId();
            //ApplicationUser user = db.Users.Include(i => i.Roles).FirstOrDefault(i => i.Id == currentUserId);    //获取当前用户TrueName，为增加流转信息提供数据
            
            var approvalItem = db.ApprovalItems.FirstOrDefault(i => i.Id == isi.Id);                //获取当前业务数据的信息

                #region 业务数据更新开始
                approvalItem.Title = isi.Title;
                approvalItem.Applicant = isi.Applicant;
                approvalItem.ApplicantPhone = isi.ApplicantPhone;
                approvalItem.Description = isi.Description;
                approvalItem.Solution = isi.Solution;
            if (isComplete == 1)
                    approvalItem.CompleteDate = DateTime.Now;
            #endregion

            #region 审批意见更新开始
            if (Opinion != null)
            {
                if (Convert.ToString(Opinion) != "")
                {
                    if (workFlowItem.WfWriteField.Trim() == "ManagerOpinion")
                    {
                        approvalItem.ManagerOpinion = approvalItem.ManagerOpinion + "<br>" + Opinion + "     (意见填写人:" + username + "    时间:" + DateTime.Now + ")";
                    }
                    if (workFlowItem.WfWriteField.Trim() == "ProcessingDepartmentOpinion")
                    {
                            approvalItem.ProcessingDepartmentOpinion = approvalItem.ProcessingDepartmentOpinion + "<br>" + Opinion + "     (意见填写人:" + username + "    时间:" + DateTime.Now + ")";
                    }

                }
            }
            #endregion
            if (workFlowItem != null)
            {
                    workFlowItem.WfCurrentUser = CurrentUser;
                    workFlowItem.Wfstatus = CurrentState;
                    workFlowItem.WfWriteField = OpinionField;
                if (isComplete == 1)
                        workFlowItem.WfCompleteDate = DateTime.Now;
                    workFlowItem.WfFlowChart = workFlowItem.WfFlowChart + "->" + trackerInstance.CurrentState + "(" + username + ")" + completeStr;          //增加流转信息
                try
                {
                    db.SaveChanges();
                    var json = new
                    {
                        okMsg = "提交成功"
                    };

                    return Json(json, "text/html", JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    var json = new
                    {
                        errorMsg = "提交失败"
                    };

                    return Json(json, "text/html", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var json = new
                {
                    errorMsg = "流程不存在"
                };

                return Json(json, "text/html", JsonRequestBehavior.AllowGet);
            }
            }
        }


        private static Activity CreateActivityFrom(string xaml)
        {

            var xamlSettings = new XamlXmlReaderSettings { LocalAssembly = Assembly.GetExecutingAssembly() };

            var xamlReader = ActivityXamlServices
                .CreateReader(new XamlXmlReader(xaml, xamlSettings));
            Activity activity = ActivityXamlServices.Load(xamlReader);
            return activity;
        }

        private static InstanceStore CreateInstanceStore()
        {
            var conn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            var store = new SqlWorkflowInstanceStore(conn)
            {
                InstanceLockedExceptionAction = InstanceLockedExceptionAction.AggressiveRetry,
                InstanceCompletionAction = InstanceCompletionAction.DeleteNothing,
                HostLockRenewalPeriod = TimeSpan.FromSeconds(20),
                RunnableInstancesDetectionPeriod = TimeSpan.FromSeconds(3)
            };
            StateMachineStateTracker.Promote(store);
            var handle = store.CreateInstanceHandle();
            var view = store.Execute(handle, new CreateWorkflowOwnerCommand(), TimeSpan.FromSeconds(60));
            store.DefaultInstanceOwner = view.InstanceOwner;

            handle.Free();

            return store;
        }
    }
}