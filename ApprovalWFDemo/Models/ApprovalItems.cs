﻿using System;
using System.ComponentModel.DataAnnotations;


namespace ApprovalWFDemo.Models
{
    public class ApprovalItem
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }
        public string Drafter { get; set; }
        public string Applicant { get; set; }      //申请人
        public string ApplicantPhone { get; set; }      //申请人电话
        public string Description { get; set; }
        public string Solution { get; set; }
        public string DealwithpeopleName { get; set; }   //处理人名字
        public string ManagerOpinion { get; set; }                   //意见域
        public string ProcessingDepartmentOpinion { get; set; }       //意见域
        public string DealwithpeopleOpinion { get; set; }             //意见域
        public DateTime CreateDate { get; set; }
        public DateTime? CompleteDate { get; set; }
    }
}