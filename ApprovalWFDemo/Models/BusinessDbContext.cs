﻿using System.Data.Entity;

namespace ApprovalWFDemo.Models
{
    public class BusinessDbContext:DbContext
    {
        public BusinessDbContext() : base("DefaultConnection")
        {

        }

        public IDbSet<WorkFlowItem> WorkFlowItems { get; set; }

        public IDbSet<ApprovalItem> ApprovalItems { get; set; }
    }
}