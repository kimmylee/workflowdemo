﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ApprovalWFDemo.Startup))]
namespace ApprovalWFDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
