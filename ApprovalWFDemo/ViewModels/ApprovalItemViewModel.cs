﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApprovalWFDemo.ViewModels
{
    public class ApprovalItemViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }

        public string Applier { get; set; }
        public DateTime ApplyDate { get; set; }

        public DateTime? CompleteDate { get;set;}

        public string Drafter { get;set;}
    }
}