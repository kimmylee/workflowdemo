﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApprovalWFDemo.ViewModels
{
    public class WorkFlowViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string WorkFlowType { get; set; }

        public string Applicant { get; set; }

        public string ApplicantPhone { get; set; }

        public DateTime ApplicantDate { get; set; }

        public string WfStatus { get; set; }

        public string WfDrafter { get; set; }

        public string WfBusinessUrl { get; set; }
    }
}