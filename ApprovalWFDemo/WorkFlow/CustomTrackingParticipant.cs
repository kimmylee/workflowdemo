﻿using System;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace ApprovalWFDemo.WorkFlow
{
    public class CustomTrackingParticipant:TrackingParticipant
    {
        public IDictionary<string, object> Outputs { get; set; }

        protected override void Track(TrackingRecord record, TimeSpan timeout)
        {
            if (record != null)
            {
                if (record is CustomTrackingRecord)
                {
                    var customTrackingRecord = record as CustomTrackingRecord;
                    Outputs = customTrackingRecord.Data;
                }
            }
        }
    }
}