﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WF.WebDemo.Models;
using WF.WebDemo.WorkFlow;

namespace WF.WebDemo.Controllers
{
    public class BaseController : Controller
    {

        public Customer customer = new Customer();



        // GET: Base

        public string GoToNextPage()
        {
            if (Session["testCustomer"] == null)
                Session["testCustomer"] = customer;
            customer = Session["testCustomer"] as Customer;
            IDictionary<string,object> wfParams= new Dictionary<string,object>();
            var currentPage = Request.UrlReferrer.AbsolutePath;
            wfParams.Add("currentPage", currentPage);
            wfParams.Add("customer", customer);
            wfParams = WorkflowInvoker.Invoke(new PageNavigationWorkflow(), wfParams);
            // var nextPage = "http://"+Request.UrlReferrer.Host+":"+Request.UrlReferrer.Port+ wfParams["nextPage"].ToString();
            var nextPage =   wfParams["nextPage"].ToString();
            return nextPage;
        }
    }
}