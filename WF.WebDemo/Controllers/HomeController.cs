﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WF.WebDemo.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult OnSelectVerticalType(string verticalType)
        {
            customer.vehicle.VehicleType = verticalType;
            var nextPageUrl= GoToNextPage();

            return Json(new {success=true,nextPage=nextPageUrl},JsonRequestBehavior.AllowGet);
        }

        public ActionResult Car()
        {
            return View();
        }
        public JsonResult OnSelectCarType(string carType,string carName)
        {
            customer.vehicle.VehicleType = "C";
            customer.vehicle.VehicleId = carType;
            customer.vehicle.VehicleName = carName;
            customer.vehicle.VehiclePrice = 1000 * Convert.ToInt32(carType);
            var nextPageUrl = GoToNextPage();

            return Json(new { success = true, nextPage = nextPageUrl }, JsonRequestBehavior.AllowGet);
        }
        

        public ActionResult Bike()
        {
            return View();
        }
        public JsonResult OnSelectBike(string bikeType, string bikeName)
        {
            customer.vehicle.VehicleType = "B";
            customer.vehicle.VehicleId = bikeType;
            customer.vehicle.VehicleName = bikeName;
            customer.vehicle.VehiclePrice = 1000 * Convert.ToInt32(bikeType);
            var nextPageUrl = GoToNextPage();

            return Json(new { success = true, nextPage = nextPageUrl }, JsonRequestBehavior.AllowGet);
        }
        

        public ActionResult Delivery()
        {
            if (customer.PaymentType == "L")
            {
                var interest = customer.Interest + "%";
                ViewBag.PaymentContent = "您的付款方式为贷款， 贷款利率为：" + interest;
            }
            else
            {
                ViewBag.PaymentContent = "您的付款方式为现金/支票";
            }
           
            return View();
        }

        public ActionResult Payment()
        {
            return View();
        }
        public JsonResult SubmitPayment(string name, string ssn, string income,string paymentType)
        {
            customer.Name = name;
            customer.SSN = ssn;
            customer.Income = Convert.ToInt32(income);
            customer.PaymentType = paymentType;
            var nextPageUrl = GoToNextPage();
            return Json(new { success = true, nextPage = nextPageUrl }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult NonDelivery()
        {
            return View();
        }
    }
}