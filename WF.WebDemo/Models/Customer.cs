﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WF.WebDemo.Models
{
    public class Customer
    {
        public Customer()
        {
            vehicle = new Vehicle();
        }
        public string Name { get; set; }
        public string SSN { get; set; }
        public int Income { get; set; }
        public string PaymentType { get; set; }
        public int Interest { get; set; }
        public Vehicle vehicle { get; set; }

    }

    public class Vehicle
    {
        public string VehicleType { get; set; }
        public string VehicleId { get; set; }
        public string VehicleName { get; set; }
        public int VehiclePrice { get; set; }
    }


    public class CreditScore
    {
        public int WhatIsTheScore(string ssn)
        {
            Random r = new Random();
            return r.Next(1, 10) * 100;
        }
    }
}