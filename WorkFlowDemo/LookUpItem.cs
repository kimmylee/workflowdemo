﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace WorkFlowDemo
{

    public sealed class LookUpItem : CodeActivity
    {
        // Define an activity input argument of type string
        public InArgument<string> ItemCode { get; set; }

        public OutArgument<ItemInfo> Item { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            // Obtain the runtime value of the Text input argument
            var itemInfo = new ItemInfo();

            string code = context.GetValue(this.ItemCode);

            itemInfo.ItemCode = code;

            switch (itemInfo.ItemCode)
            {
                case "A":
                    itemInfo.Description = "A";
                    itemInfo.Price = 10;
                    break;
                case "B":
                    itemInfo.Description = "B";
                    itemInfo.Price = 20;
                    break;
                case "C":
                    itemInfo.Description = "C";
                    itemInfo.Price = 30;
                    break;
            }
            context.SetValue(this.Item,itemInfo);
        }
    }
}
