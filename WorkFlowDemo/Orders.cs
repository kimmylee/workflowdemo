﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowDemo
{
    public class Orders
    {
        public Orders()
        {
            Items= new List<OrderItem>();
        }

        public int OrderId { get; set; }

        public string Description { get; set; }

        public int TotalWeight { get; set; }

        public string MethodName { get; set; }

        public IList<OrderItem> Items { get; set; }


    }

    public class OrderItem
    {
        public int OrderItemId { get; set; }

        public string OrderItemCode { get; set; }
        
        public int Quantity { get; set; }

        public decimal Price { get; set; }
    }
}
