﻿using System;
using System.Linq;
using System.Activities;
using System.Activities.Expressions;
using System.Activities.Statements;
using System.Collections.Generic;

namespace WorkFlowDemo
{

    class Program
    {
        //static void Main(string[] args)
        //{
        //    var order = new Orders
        //    {
        //        OrderId = 1,
        //        Description = "控制流",
        //        MethodName = "C",
        //        TotalWeight = 1,
        //        Items = new List<OrderItem>
        //        {
        //            new OrderItem
        //            {
        //                OrderItemId = 1,
        //                OrderItemCode = "1",
        //                Price = 30,
        //                Quantity = 2
        //            },
        //            new OrderItem
        //            {
        //                OrderItemId = 2,
        //                OrderItemCode = "2",
        //                Price = 20,
        //                Quantity = 2
        //            },
        //            new OrderItem
        //            {
        //                OrderItemId = 3,
        //                OrderItemCode = "3",
        //                Price = 40,
        //                Quantity = 2
        //            },
        //        }
        //    };

        //    IDictionary<string, object> keys = new Dictionary<string, object>();
        //    keys.Add("MyOrder", order);

        //    WorkflowInvoker.Invoke(new OrderProcess(), keys);
        //    WorkflowInvoker.Invoke(new LookUp());
        //    Console.WriteLine("----------------------------------------");
        //    Console.ReadLine();

        //}


        static void Main(string[] args)
        {
            var item = new ItemInfo
            {
                ItemCode = "A",
                Description = "Description For A",
                Price = 30
            };

            IDictionary<string, object> keys = new Dictionary<string, object>();
            keys.Add("MyItem", item);

            WorkflowInvoker.Invoke(new LookUp(), keys);
            //WorkflowInvoker.Invoke(new LookUp());
            Console.WriteLine("----------------------------------------");
            Console.ReadLine();
        }

    }

    //public class MyCodeWorkFlow
    //{
    //    public static Activity CreateWorkFlow()
    //    {
    //        Variable<int> count = new Variable<int>()
    //        {
    //            Name = "count",
    //            Default = 0
    //        };
    //        Variable<int> timeNowHour = new Variable<int>()
    //        {
    //            Name = "timeNowHour",
    //            Default = DateTime.Now.Hour
    //        };

    //        return new Sequence()
    //        {
    //            DisplayName = "Main Sequence",
    //            Variables = { count,timeNowHour},
    //            Activities = {  new WriteLine()
    //            {
    //                DisplayName = "问候",
    //                Text = "Hello, this is come form code activity!"
    //            } ,
    //            new If()
    //            {
    //                DisplayName = "判断",
    //                Condition = ExpressionServices.Convert<bool>(u =>timeNowHour.Get(u)>12),
    //                Then = new Assign<int>()
    //                {
    //                    To = timeNowHour,
    //                    Value = new InArgument<int>(m=>timeNowHour.Get(m)-12)
    //                }
    //            }
    //            }
    //        }
    //    }

    //}
}
